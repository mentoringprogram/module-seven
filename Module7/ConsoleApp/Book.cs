﻿using System;
using System.Collections.Generic;
using MongoDB.Bson;

namespace ConsoleApp
{
    internal class Book
    {
        public ObjectId Id { get; set; }
        public string Name { get; set; }
        public string Author { get; set; }
        public int Count { get; set; }
        public List<string> Genre { get; set; }
        public DateTime Year { get; set; }
    }
}